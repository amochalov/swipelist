package com.artemych.swipelist;


import android.app.Application;

public class App extends Application {

    private static App instance;

    private ApplicationInjector injector = new ApplicationInjector();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    ApplicationInjector getInjector() {
        return injector;
    }

    static App app() {
        return instance;
    }
}
