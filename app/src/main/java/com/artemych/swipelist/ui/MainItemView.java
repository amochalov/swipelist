package com.artemych.swipelist.ui;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.artemych.swipelist.R;
import com.artemych.swipelist.data.MainDataItem;

public class MainItemView extends ItemView<MainDataItem> {

    private static final int OPEN_ROTATION = 0;
    private static final int CLOSE_ROTATION = 180;
    private static final int ROTATION_ANIMATION_DURATION = 150;
    private final View openButton;

    public MainItemView(Context context) {
        super(context);
        openButton = findViewById(R.id.open_button);
    }

    @Override
    public void bind(MainDataItem dataItem) {
        super.bind(dataItem);
        openButton.setVisibility(dataItem.hasSubItems() ? VISIBLE : GONE);
        openButton.setRotation(dataItem.isOpen() ? OPEN_ROTATION : CLOSE_ROTATION);
    }

    @Override
    public void setItemViewListener(ItemViewListener listener) {
        super.setItemViewListener(listener);
        View.OnClickListener clickListener = view -> {
            MainDataItem mainDataItem = getBoundItem();
            if (mainDataItem != null && listener != null) {
                animateOpenButton(!mainDataItem.isOpen());
                ((MainItemViewListener)listener).onOpenButtonClicked(mainDataItem);
            }
        };
        openButton.setOnClickListener(clickListener);
        swipeLayout.setOnClickListener(clickListener);
    }

    private void animateOpenButton(boolean open) {
        openButton.clearAnimation();
        openButton
                .animate()
                .rotation(open ? OPEN_ROTATION : CLOSE_ROTATION)
                .setInterpolator(new AccelerateInterpolator())
                .setDuration(ROTATION_ANIMATION_DURATION)
                .start();
    }

    public interface MainItemViewListener extends ItemViewListener {
        void onOpenButtonClicked(@NonNull MainDataItem dataItem);
    }
}
