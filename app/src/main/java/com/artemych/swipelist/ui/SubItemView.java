package com.artemych.swipelist.ui;


import android.content.Context;

import com.artemych.swipelist.data.SubDataItem;

public class SubItemView extends ItemView<SubDataItem> {

    public SubItemView(Context context) {
        super(context);
        swipeLayout.setBackgroundColor(context.getResources().getColor(android.R.color.darker_gray));
    }
}
