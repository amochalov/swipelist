package com.artemych.swipelist.ui;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.artemych.swipelist.R;
import com.artemych.swipelist.data.DataItem;

public abstract class ItemView<T extends DataItem> extends FrameLayout {

    private static final int DEFAULT_ANIMATION_DURATION = 100;
    private static final int REMOVE_ANIMATION_DURATION = 75;

    @Nullable
    private ItemViewListener listener;

    @Nullable
    private T boundItem;
    private final TextView title;
    private final View removeView;
    protected final View swipeLayout;
    private int downX;
    private int downY;
    private float lastMoveX;
    private boolean isInSwipe;
    private float initialTranslation;
    private final int width;
    private float flingXVelocity;
    private boolean leftToRightSwipe;

    public ItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_view, this);
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        title = findViewById(R.id.item_title);
        width = getResources().getDisplayMetrics().widthPixels;
        swipeLayout = findViewById(R.id.swipe_layout);
        removeView = findViewById(R.id.remove_background);
        initSwipeLayoutTouch(context);
    }

    @CallSuper
    public void bind(T dataItem) {
        boundItem = dataItem;
        title.setText(dataItem.getItemTitle());
        swipeLayout.setTranslationX(dataItem.isSwiped() ? -width / 2 : 0);
    }

    public void setItemViewListener(ItemViewListener listener) {
        this.listener = listener;
        removeView.setOnClickListener(view -> {
            if (boundItem != null) {
                listener.onRemove(boundItem);
            }
        });
    }

    @Nullable
    public T getBoundItem() {
        return boundItem;
    }

    private void initSwipeLayoutTouch(Context context) {
        float touchSlope = ViewConfiguration.get(context).getScaledTouchSlop();
        GestureDetector detector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return boundItem != null && !boundItem.isSwiped() && swipeLayout.performClick();
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                flingXVelocity = Math.abs(velocityX / 1000);
                return true;
            }
        });
        //noinspection Convert2Lambda
        swipeLayout.setOnTouchListener(new OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent ev) {
                int x = (int)ev.getRawX();
                int y = (int)ev.getRawY();
                boolean result = detector.onTouchEvent(ev);
                switch (ev.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        result |= onActionDown(x, y);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        result |= onActionMove(x, y, touchSlope);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        result |= onActionUpAndCancel();
                        break;
                }
                return result;
            }
        });
    }

    private boolean onActionMove(int x, int y, float touchSlope) {
        int deltaX = downX - x;
        int deltaY = downY - y;
        if (Math.abs(deltaX) > Math.abs(deltaY) && Math.abs(deltaX) > touchSlope) {
            isInSwipe = true;
        }
        leftToRightSwipe = lastMoveX < x;
        lastMoveX = x;
        if (isInSwipe) {
            requestDisallowInterceptTouchEvent(true);
            float newTranslation = Math.min(-(downX - x) + initialTranslation, 0);
            swipeLayout.setTranslationX(newTranslation);
            return true;
        }
        return false;
    }

    private boolean onActionDown(int x, int y) {
        downX = x;
        downY = y;
        initialTranslation = swipeLayout.getTranslationX();
        return true;
    }

    private boolean onActionUpAndCancel() {
        int translation = (int)Math.abs(swipeLayout.getTranslationX());
        int initialTranslation = translation;
        if (flingXVelocity != 0) {
            int velocityTranslation = (int)(flingXVelocity * DEFAULT_ANIMATION_DURATION);
            if (leftToRightSwipe) {
                velocityTranslation *= -1;
            }
            translation += velocityTranslation;
        }

        int endTranslation = 0;
        int halfWidth = width / 2;
        int quoteWidth = width / 4;
        if (translation > halfWidth) {
            endTranslation = -width;
        } else if (translation <= halfWidth && translation > quoteWidth / 2) {
            if (leftToRightSwipe && translation < halfWidth - quoteWidth / 2) {
                endTranslation = 0;
            } else {
                endTranslation = -halfWidth;
            }
        }
        int time = correctTime(initialTranslation, endTranslation);
        swipeLayout.clearAnimation();
        swipeLayout.animate()
                .translationX(endTranslation)
                .setDuration(time)
                .setInterpolator(new LinearInterpolator())
                .withEndAction(this::onSwipeEnd);
        flingXVelocity = 0;
        isInSwipe = false;
        leftToRightSwipe = false;
        lastMoveX = 0;
        requestDisallowInterceptTouchEvent(false);
        return true;
    }

    private int correctTime(int initialTranslation, int endTranslation) {
        int time = endTranslation == - width ? REMOVE_ANIMATION_DURATION : DEFAULT_ANIMATION_DURATION;
        if (flingXVelocity != 0) {
            float defaultSpeed = Math.abs(initialTranslation - Math.abs(endTranslation)) / (float)time;
            if (defaultSpeed < flingXVelocity) {
                time = (int)(time * (defaultSpeed / flingXVelocity));
            }
        }
        return time;
    }

    private void onSwipeEnd() {
        int translation = (int)Math.abs(swipeLayout.getTranslationX());
        if (listener != null && boundItem != null) {
            if (translation == 0) {
                listener.onSwiped(boundItem, false);
            } else if (translation == getMeasuredWidth() / 2) {
                listener.onSwiped(boundItem, true);
            } else {
                listener.onRemove(boundItem);
            }
        }
    }

    public interface ItemViewListener {
        void onSwiped(@NonNull DataItem dataItem, boolean swiped);
        void onRemove(@NonNull DataItem dataItem);
    }
}
