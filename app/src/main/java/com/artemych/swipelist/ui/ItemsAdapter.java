package com.artemych.swipelist.ui;


import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.artemych.swipelist.data.DataItem;

import java.util.ArrayList;
import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {

    private final AdapterListener listener;
    private final List<DataItem> items = new ArrayList<>();

    public ItemsAdapter(AdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemView itemView;
        if (viewType == DataItem.ITEM_TYPE_MAIN) {
            itemView = new MainItemView(parent.getContext());
        } else {
            itemView = new SubItemView(parent.getContext());
        }
        itemView.setItemViewListener(listener);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getItemType();
    }

    public void onItemsInserted(int fromIndex, List<? extends DataItem> insertedItems) {
        items.addAll(fromIndex, insertedItems);
        notifyItemRangeInserted(fromIndex, insertedItems.size());
    }

    public void onItemsRemoved(int fromIndex, List<? extends DataItem> removedItems) {
        items.removeAll(removedItems);
        notifyItemRangeRemoved(fromIndex, removedItems.size());
    }

    public void onItemChanged(DataItem dataItem) {
        notifyItemChanged(items.indexOf(dataItem));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(ItemView itemView) {
            super(itemView);
        }

        void bind(DataItem dataItem) {
            //noinspection unchecked
            ((ItemView) itemView).bind(dataItem);
        }
    }

    public interface AdapterListener extends MainItemView.MainItemViewListener {}
}
