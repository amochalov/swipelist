package com.artemych.swipelist.data;


public class MainDataItem extends DataItem {

    private boolean open;
    private boolean hasSubItems = true;

    MainDataItem(String title) {
        super(title);
    }

    public boolean isOpen() {
        return open;
    }

    void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public int getItemType() {
        return DataItem.ITEM_TYPE_MAIN;
    }

    public boolean hasSubItems() {
        return hasSubItems;
    }

    void setNoSubItems() {
        this.hasSubItems = false;
    }
}
