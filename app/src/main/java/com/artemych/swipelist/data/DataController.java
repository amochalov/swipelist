package com.artemych.swipelist.data;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.artemych.swipelist.ListenerCord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class DataController {

    private static final int MAIN_ITEMS_COUNT = 100;
    private static final int MAX_SUB_ITEMS_COUNT = 3;
    private static final String SUB_ITEM_TITLE_PREFIX = "Sub item ";
    private static final String MAIN_ITEM_TITLE_PREFIX = "Main item ";

    private final List<DataItem> items = new ArrayList<>(MAIN_ITEMS_COUNT);
    private final Map<MainDataItem, List<SubDataItem>> mainToSubItemsMap = new HashMap<>(MAIN_ITEMS_COUNT);

    @Nullable
    private DataChangeListener listener;

    public void fill() {
        Random random = new Random();
        for (int i = 0; i < MAIN_ITEMS_COUNT; i++) {
            MainDataItem dataItem = new MainDataItem(MAIN_ITEM_TITLE_PREFIX + (i + 1));
            items.add(dataItem);
            mainToSubItemsMap.put(dataItem, generateSubItems(random));
        }
    }

    @NonNull
    public ListenerCord attachListener(DataChangeListener listener) {
        this.listener = listener;
        notifyItemsInserted(0, items);
        return () -> this.listener = null;
    }

    public void remove(DataItem dataItem) {
        if (dataItem.getItemType() == DataItem.ITEM_TYPE_MAIN) {
            remove((MainDataItem)dataItem);
        } else {
            remove((SubDataItem) dataItem);
        }
    }

    private void remove(MainDataItem dataItem) {
        int index = items.indexOf(dataItem);
        if (index < 0) {
            return;
        }
        items.remove(dataItem);
        List<DataItem> removedItems = new ArrayList<>();
        removedItems.add(dataItem);
        List<SubDataItem> subItems = mainToSubItemsMap.remove(dataItem);
        if (subItems != null && dataItem.isOpen()) {
            items.removeAll(subItems);
            removedItems.addAll(subItems);
        }
        notifyItemsRemoved(index, removedItems);
    }

    private void remove(SubDataItem dataItem) {
        int index = items.indexOf(dataItem);
        if (index < 0) {
            return;
        }
        for (int i = index - 1; i > -1 ; i--) {
            DataItem possibleParent = items.get(i);
            if (possibleParent.getItemType() == DataItem.ITEM_TYPE_MAIN) {
                items.remove(dataItem);
                MainDataItem parent = (MainDataItem)possibleParent;
                List<SubDataItem> subDataItems = mainToSubItemsMap.get(parent);
                subDataItems.remove(dataItem);
                if (subDataItems.isEmpty()) {
                    mainToSubItemsMap.remove(parent);
                    parent.setNoSubItems();
                    notifyItemChanged(parent);
                }
                break;
            }
        }
        notifyItemsRemoved(index, Collections.singletonList(dataItem));
    }

    public void openItem(MainDataItem dataItem) {
        if (dataItem.isOpen()) {
            return;
        }
        setItemOpened(dataItem, true);
    }

    public void closeItem(MainDataItem dataItem) {
        if (!dataItem.isOpen()) {
            return;
        }
        setItemOpened(dataItem, false);
    }

    private void setItemOpened(MainDataItem dataItem, boolean opened) {
        dataItem.setOpen(opened);
        int index = items.indexOf(dataItem) + 1;
        List<? extends DataItem> subItems = mainToSubItemsMap.get(dataItem);
        if (subItems == null) {
            return;
        }
        if (opened) {
            items.addAll(index, subItems);
            notifyItemsInserted(index, subItems);
        } else {
            items.removeAll(subItems);
            notifyItemsRemoved(index, subItems);
        }
    }

    public void setItemSwiped(DataItem dataItem, boolean swiped) {
        dataItem.setSwiped(swiped);
    }

    private List<SubDataItem> generateSubItems(Random random) {
        int count = random.nextInt(MAX_SUB_ITEMS_COUNT) + 1;
        List<SubDataItem> list = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            list.add(new SubDataItem(SUB_ITEM_TITLE_PREFIX + (i + 1)));
        }
        return list;
    }

    private void notifyItemsInserted(int index, List<? extends DataItem> items) {
        if (listener != null) {
            listener.onItemsInserted(index, items);
        }
    }

    private void notifyItemsRemoved(int index, List<? extends DataItem> items) {
        if (listener != null) {
            listener.onItemsRemoved(index, items);
        }
    }

    private void notifyItemChanged(DataItem parent) {
        if (listener != null) {
            listener.notifyItemChanged(parent);
        }
    }

    public interface DataChangeListener {
        void onItemsInserted(int fromIndex, List<? extends DataItem> items);
        void onItemsRemoved(int fromIndex, List<? extends DataItem> items);
        void notifyItemChanged(DataItem dataItem);
    }
}
