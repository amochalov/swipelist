package com.artemych.swipelist.data;


import java.util.concurrent.atomic.AtomicInteger;

public abstract class DataItem {

    public static final int ITEM_TYPE_MAIN = 1;
    static final int ITEM_TYPE_SUB = 2;

    private static final AtomicInteger ID_GENERATOR = new AtomicInteger();

    private final int id = ID_GENERATOR.incrementAndGet();
    private final String title;
    private boolean swiped;

    DataItem(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public boolean isSwiped() {
        return swiped;
    }

    void setSwiped(boolean swiped) {
        this.swiped = swiped;
    }

    public abstract int getItemType();

    public String getItemTitle() {
        return title;
    }
}
