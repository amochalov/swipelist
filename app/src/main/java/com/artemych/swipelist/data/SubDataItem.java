package com.artemych.swipelist.data;


public class SubDataItem extends DataItem {

    SubDataItem(String title) {
        super(title);
    }

    @Override
    public int getItemType() {
        return DataItem.ITEM_TYPE_SUB;
    }
}
