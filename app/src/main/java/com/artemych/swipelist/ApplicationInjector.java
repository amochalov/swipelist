package com.artemych.swipelist;


import com.artemych.swipelist.data.DataController;

class ApplicationInjector {

    private DataController controller = new DataController();

    ApplicationInjector() {
        controller.fill();
    }

    void inject(MainActivity activity) {
        activity.dataController = controller;
    }
}
