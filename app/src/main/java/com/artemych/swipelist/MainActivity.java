package com.artemych.swipelist;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.artemych.swipelist.data.DataController;
import com.artemych.swipelist.data.DataItem;
import com.artemych.swipelist.data.MainDataItem;
import com.artemych.swipelist.ui.ItemsAdapter;

import java.util.List;

import static com.artemych.swipelist.App.app;

public class MainActivity extends Activity {

    @Nullable
    private ListenerCord cord;

    DataController dataController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app().getInjector().inject(this);
        ItemsAdapter adapter = getItemsAdapter();
        initRecycler(adapter);
        cord = dataController.attachListener(new DataController.DataChangeListener() {
            @Override
            public void onItemsInserted(int fromIndex, List<? extends DataItem> items) {
                adapter.onItemsInserted(fromIndex, items);
            }

            @Override
            public void onItemsRemoved(int fromIndex, List<? extends DataItem> items) {
                adapter.onItemsRemoved(fromIndex, items);
            }

            @Override
            public void notifyItemChanged(DataItem dataItem) {
                adapter.onItemChanged(dataItem);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cord != null) {
            cord.unregister();
            cord = null;
        }
    }

    @NonNull
    private ItemsAdapter getItemsAdapter() {
        return new ItemsAdapter(new ItemsAdapter.AdapterListener() {
            @Override
            public void onOpenButtonClicked(@NonNull MainDataItem dataItem) {
                if (dataItem.isOpen()) {
                    dataController.closeItem(dataItem);
                } else {
                    dataController.openItem(dataItem);
                }
            }

            @Override
            public void onSwiped(@NonNull DataItem dataItem, boolean swiped) {
                dataController.setItemSwiped(dataItem, swiped);
            }

            @Override
            public void onRemove(@NonNull DataItem dataItem) {
                dataController.remove(dataItem);
            }
        });
    }

    private void initRecycler(ItemsAdapter adapter) {
        RecyclerView list = findViewById(R.id.item_list);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setHasFixedSize(true);
        list.setAdapter(adapter);
    }
}
